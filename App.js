/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
 import 'react-native-gesture-handler';
 import React from 'react';
 import type { Node } from 'react';
 import HomeScreen from './src/Screen/HomeScreen';
 import DetailsScreen from './src/Screen/DetailsScreen';
 import CardScreen from './src/Screen/CardScreen';
 import LoadingScreen from './src/Screen/LoadingScreen';
 import {
   View,
   Text,Image
 } from 'react-native';
 import { NavigationContainer } from '@react-navigation/native';
 import { createStackNavigator } from '@react-navigation/stack';
 
 const Stack = createStackNavigator();            //LatihanNavigation
 
 const App: () => Node = () => {
   return (
     <NavigationContainer>
       <Stack.Navigator>
         <Stack.Screen name="Loading" component={LoadingScreen} />
         <Stack.Screen name="Home" component={HomeScreen} />
         <Stack.Screen name="Details" component={DetailsScreen} />
         <Stack.Screen name="Card" component={CardScreen} />
       </Stack.Navigator>
     </NavigationContainer>
   );
 };
 
 export default App;
























 
// import React, { Component } from 'react'
// import { Text, View, Image, StyleSheet, ScrollView } from 'react-native'

// export default class Baru1 extends Component {
//   constructor() {
//     super()
//     this.state = { 
//       name:'ario', 
//       alamat:'gempol', 
//       data: [],
//     }
//   }

//   render() {
//     return (
//       <View style={styles.container}>
//         <View style={styles.table}>
//           <View style={styles.header}>
//             <Text style={styles.header}></Text>
//               <Text style={styles.otherTitle}>Nama</Text>
//               <Text style={styles.otherTitle}>Alamat</Text>
//           </View>
//           {this.state.data.map((value, index)} => {
//             return(
//             <View key={index} style={styles.isi}>
//               <Text style={[styles.number, styles.data]}> {index+1} </Text>
//               <Text style={[styles.otherTitle, styles.data]}> {value.nama} </Text>
//               <Text style={[styles.otherTitle, styles.data]}> {value.alamat} </Text>
//             </View>
//           )
//           })}
//         </View>
//       </View>
//           )
//         }
//       }
//       const styles = StyleSheet.create({
//         container: {
//           flex: 1,
//           backgroundColor: 'white',
//         },
//       });

























































// import React, { Component } from 'react'
// import { Text, View, Image, StyleSheet, ScrollView } from 'react-native'


// export default class Baru1 extends Component {
//   constructor() {
//     super()
//     this.state = { 
//       name:'ario', 
//       alamat:'gempol', 
//       data: [],
//     }
//   }

//   render() {
//     return (
//       <ScrollView style={styles.scrollView}>
//         <View style={{ flex: 1, backgroundColor: 'white' }}>
//           <View style={styles.container}>
//             <View style={{ flex: .05, justifyContent: 'center', alignItems: 'center' }}>
//               <View style={{ width: 400, height: 120, backgroundColor: '#191970', marginTop: 20 }}>
//                 <Text style={{ color: 'white', fontSize: 50, marginTop: 20, alignSelf: 'center', justifyContent: 'center' }}> Data Diri </Text>
//               </View>
//             </View>
//             <View style={{ flex: .1, flexDirection: 'column', marginVertical: 20, justifyContent: 'space-between', marginLeft: 10, marginRight: 10, marginBottom: 10 }}>
//             {this.state.data.map((value, index) => (
//               <View key={index} style={{ width: 330, height: 400, backgroundColor: '#191970' }}>
//                 <Image style={{ width: '100%', height: "80%", justifyContent: 'center' }} source={{uri:value.Poster}} />
//                 <Text style={{ color: 'white', alignSelf: 'center', fontSize: 17 }}>{value.Title}</Text>
//                 <Text style={{ color: 'white', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent: 'center',flexDirection: 'column' }}>{value.Year}</Text>
//                 <Text style={{ color: 'white', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent: 'center',flexDirection: 'column' }}>{value.Type}</Text>
//               </View>
//                 ))}
//                   <Text>{this.state.name}</Text>
//                   <Text>{this.state.alamat}</Text>
//             </View>
//           </View>
//         </View>
//       </ScrollView>
//     )
//   }
// }
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: 'white',
//   },
// });

 























































// `import React, { Component } from 'react'
// import { Text, View, Image, StyleSheet, ScrollView } from 'react-native'


// export default class Baru1 extends Component {
    
//   render() {
//     return (
//         <ScrollView style={styles.scrollView}>
        
//       <View style={{flex: 1, backgroundColor:'white'}}>
          
//          <View style={styles.container}>
                
                
//         <View style={{flex: .05, justifyContent: 'center', alignItems: 'center', marginBottom: 110}}>
        
//         <View style={{width:400, height:120, backgroundColor:'#ffebcd',marginTop:40}}>
//           <Text style={{ color: 'black', fontSize: 50, marginTop: 20, alignSelf: 'center', justifyContent:'center' }}> Movies card </Text>
//           </View>
        
//         </View>
//         <View style={{flex: .1, flexDirection:'row', marginVertical: 20, justifyContent:'space-between', marginLeft:10, marginRight:10,marginBottom:10}}>
//           <View style={{width:160, height:250, backgroundColor:'#ffebcd'}}>
//           <Image style={{ width: '100%', height: "80%",justifyContent:'center' }} source={require('./src/assets/movies1.png')} />
//           <Text style={{ color: 'black', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent:'center' }}> the rainbow troops </Text>
//           </View>
//           <View style={{width:160, height:250, backgroundColor:'#ffebcd'}}>
//           <Image style={{ width: '100%', height: "80%",justifyContent:'center' }} source={require('./src/assets/moviee2.png')} />
//           <Text style={{ color: 'black', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent:'center' }}> someday we'll talk about today </Text>
//           </View>
//         </View>

//         <View style={{flex: .1, flexDirection:'row', marginVertical: 20, justifyContent:'space-between', marginLeft:10, marginRight:10,marginTop:70}}>
//           <View style={{width:160, height:250, backgroundColor:'#ffebcd'}}>
//                 <Image style={{ width: '100%', height: "80%",justifyContent:'center' }} source={require('./src/assets/movies3.png')} />
//                 <Text style={{ color: 'black', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent:'center' }}> wedding rings </Text>
//                 </View>
//           <View style={{width:160, height:250, backgroundColor:'#ffebcd'}}>
//           <Image style={{ width: '100%', height: "80%",justifyContent:'center' }} source={require('./src/assets/movies4.png')} />
//           <Text style={{ color: 'black', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent:'center' }}> incredible loves </Text>
//           </View>
          
//         </View>

//         </View>
//       </View>
//       </ScrollView>
//     )
//   }
// }
// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
        
//         backgroundColor: '#deb887',
//     },
// });























































// import React, {Component} from 'react'
// import {  Text, View  } from 'react-native'
// import StateLatihan from './src/Container/State'
// import Icon from 'react-native-vector-icons/FontAwesome5';

// class App extends Component {
//   render() {
//     return (
//        <StateLatihan/>

//     )
//   }
// }
// export default App
























// // import React, { Component } from 'react'
// // import { Text, View, Image, StyleSheet, ScrollView } from 'react-native'


// // export default class Baru1 extends Component {
// //   render() {
// //     return (
// //         <ScrollView style={styles.scrollView}>
        
// //       <View style={{flex: 1, backgroundColor:'white'}}>
          
// //          <View style={styles.container}>
                
                
// //         <View style={{flex: .05, justifyContent: 'center', alignItems: 'center', marginBottom: 110}}>
        
// //         <View style={{width:400, height:120, backgroundColor:'#ffebcd',marginTop:40}}>
// //           <Text style={{ color: 'black', fontSize: 50, marginTop: 20, alignSelf: 'center', justifyContent:'center' }}> Movies card </Text>
// //           </View>
        
// //         </View>
// //         <View style={{flex: .1, flexDirection:'row', marginVertical: 20, justifyContent:'space-between', marginLeft:10, marginRight:10,marginBottom:10}}>
// //           <View style={{width:160, height:250, backgroundColor:'#ffebcd'}}>
// //           <Image style={{ width: '100%', height: "80%",justifyContent:'center' }} source={require('./src/assets/movies1.png')} />
// //           <Text style={{ color: 'black', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent:'center' }}> the rainbow troops </Text>
// //           </View>
// //           <View style={{width:160, height:250, backgroundColor:'#ffebcd'}}>
// //           <Image style={{ width: '100%', height: "80%",justifyContent:'center' }} source={require('./src/assets/moviee2.png')} />
// //           <Text style={{ color: 'black', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent:'center' }}> someday we'll talk about today </Text>
// //           </View>
// //         </View>

// //         <View style={{flex: .1, flexDirection:'row', marginVertical: 20, justifyContent:'space-between', marginLeft:10, marginRight:10,marginTop:70}}>
// //           <View style={{width:160, height:250, backgroundColor:'#ffebcd'}}>
// //                 <Image style={{ width: '100%', height: "80%",justifyContent:'center' }} source={require('./src/assets/movies3.png')} />
// //                 <Text style={{ color: 'black', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent:'center' }}> wedding rings </Text>
// //                 </View>
// //           <View style={{width:160, height:250, backgroundColor:'#ffebcd'}}>
// //           <Image style={{ width: '100%', height: "80%",justifyContent:'center' }} source={require('./src/assets/movies4.png')} />
// //           <Text style={{ color: 'black', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent:'center' }}> incredible loves </Text>
// //           </View>
          
// //         </View>

// //         </View>
// //       </View>
// //       </ScrollView>
// //     )
// //   }
// // }
// // const styles = StyleSheet.create({
// //     container: {
// //         flex: 1,
        
// //         backgroundColor: '#deb887',
// //     },
// // });
















// import React, { Component } from 'react';
// import { Alert, Button, TextInput, View, StyleSheet, Image, Text } from 'react-native';
// import Icon from 'react-native-vector-icons/FontAwesome5';

// export default class App extends Component {
//     constructor(props) {
//         super(props);

//         this.state = {
//             username: '',
//             password: '',
//         };
//     }

//     onLogin = () => {
//         const { username, password } = this.state;

//         Alert.alert('Credentials', `${username} + ${password}`);
//     }

//     render() {
//         return (


// <View style={styles.container}>
    // <Image style={{ width: 10, height: 10, padding: 70 }}
//         resizeMode={'center'}
//         source={require('./src/assets/Salt1.png')} style={styles.logo} />

//                     <Text style={{ color: 'black', fontSize: 16 }}> Please login with a registered session </Text>
//                     <View style={{ flex: .1, justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
//                     </View>

//                 <View style={styles.input}>
//                     <Icon name="user" size={22} color="#000000" />
//                     <TextInput
//                         value={this.state.username}
//                         onChangeText={(username) => this.setState({ username })}             
//                         placeholder={'Username'}
//                         placeholderTextColor={'black'}
//                         color={'black'}
//                     />
//                 </View>

//                 <View style={styles.input}>
//                     <Icon name="key" size={22} color="#000000" />
//                     <TextInput
//                         value={this.state.username}
//                         onChangeText={(username) => this.setState({ username })}
//                         placeholder={'Password'}
//                         placeholderTextColor={'black'}
//                         color={'black'}
//                     />
//                 </View>





// {/* <Icon style={styles.input} name="key" size={30} color="#000000" />
//                     <TextInput

//                     value={this.state.password}
//                     onChangeText={(password) => this.setState({ password })}
//                     placeholder={'Password'}
//                     placeholderTextColor={'black'}
//                     secureTextEntry={true}
//                     style={styles.input}
//                 /> */}

//                     {/* <Button
//                     title={'Login'}
//                     style={styles.input}
//                     onPress={this.onLogin()}
//                 />
//                 <View style={{}}>
//                     <Text>Forgot Password?</Text>

//                 </View>
//             </View>
//         );
//     }
// }

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         alignItems: 'center',
//         justifyContent: 'center',
//         backgroundColor: 'yellow',
//     },
//     input: {
//         width: 200,
//         height: 60,
//         padding: 10,
//         flexDirection: "row",
//         borderWidth: 1,
//         borderColor: 'black',
//         elevation: 10,
//         backgroundColor: '#ecf0f1',
//         marginBottom: 20,
//     },
// }); */}









// import React, { Component } from 'react';
// import { Alert, Button, TextInput, View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
// import Icon from 'react-native-vector-icons/FontAwesome5';

// export default class App extends Component {
//     render() {
//         return (
            
            
//             <View style={styles.container}>
//                 <Image style={{ width: '34%', height: "8%", alignItems:'center', marginBottom:40, }} source={require('./src/assets/Salt1.png')} />
//                 <Text style={{ color: 'black', fontSize: 13,marginBottom:12 }}> Please Register with valid data </Text>
//                 <View style={styles.input}>
//                     <Icon name="user" size={22} color="#000000" />
//                     <TextInput
//                         placeholder='Fullname'
//                         placeholderTextColor='black'
//                         color='black'
                        
//                     />
//                 </View>
//                 <View style={styles.input}>
//                     <Icon name="user" size={22} color="#000000" />
//                     <TextInput
//                         placeholder='Username'
//                         placeholderTextColor='black'
//                         color='black'
//                     />
//                 </View>
//                 <View style={styles.input}>
//                     <Icon name="envelope" size={22} color="#000000" />
//                     <TextInput
//                         placeholder='Email'
//                         placeholderTextColor='black'
//                         color='black'
//                     />
//                 </View>
//                 <View style={styles.input}>
//                     <Icon name="key" size={22} color="#000000" />
//                     <TextInput
//                         placeholder='Password'
//                         placeholderTextColor='black'
//                         color='black'
//                     />
//                 </View>
//                 <View style={styles.input}>
//                     <Icon name="key" size={22} color="#000000" />
//                     <TextInput
//                         placeholder='Confirm Password'
//                         placeholderTextColor='black'
//                         color='black'
//                     />
//                 </View>
//                 <Button
//                     title='Register'
//                     style={styles.input}
//                     onPress={()=>alert('Register Berhasil')}
//                 />
//                 <TouchableOpacity onPress={()=>alert('Masukkan email')}>
//                     <Text>Forgot Password?</Text>
                    
//                 </TouchableOpacity>
//             </View>
//         );
//     }                            //Latian7
// }

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         alignItems: 'center',
//         justifyContent: 'center',
//         backgroundColor: 'yellow',
//     },
//     input: {
//         alignItems:'center',
//         width: 220,
//         height: 60,
//         padding: 10,
//         flexDirection: "row",
//         borderWidth: 2,
//         borderColor: 'black',
//         elevation: 10,
//         backgroundColor: '#ecf0f1',
//         marginBottom: 20,
        
//     },
// });




// import React, { Component } from 'react'
// import { Text, View, Image, StyleSheet, ScrollView } from 'react-native'


// export default class Baru1 extends Component {
//   constructor() {
//     super()
//     this.state = { 
//       name:'ario',
//       totalResult:'0', 
//       alamat:'gempol', 
//       data: [],
//     }
//   }

//   componentDidMount = () => {
//     console.log([0,1,2,3,4,5,6,7,8,9,19] )
//     console.log(typeof View)
  
//     fetch('http://www.omdbapi.com/?s=avengers&apikey=997061b4&')
//       .then(response => response.json())
//       .then(jjson => {
//         console.log(jjson.Search[0].Year)
//         this.setState({ data: [jjson.Search[8],jjson.Search[7],jjson.Search[6]] })
//       })

//       this.setState({ name:'', alamat:'gempol'})
//   }

//   render() {
//     return (
//       <ScrollView style={styles.scrollView}>
//         <View style={{ flex: 1, backgroundColor: 'white' }}>
//           <View style={styles.container}>
//             <View style={{ flex: .05, justifyContent: 'center', alignItems: 'center' }}>
//               <View style={{ width: 400, height: 120, backgroundColor: '#191970', marginTop: 20 }}>
//                 <Text style={{ color: 'white', fontSize: 50, marginTop: 20, alignSelf: 'center', justifyContent: 'center' }}> Movies card </Text>
//               </View>
//             </View>
//             <View style={{ flex: .1, flexDirection: 'column', marginVertical: 20, justifyContent: 'space-between', marginLeft: 10, marginRight: 10, marginBottom: 10 }}>
//             {this.state.data.map((value, index) => (
//               <View key={index} style={{ width: 330, height: 400, backgroundColor: '#191970' }}>
//                 <Image style={{ width: '100%', height: "80%", justifyContent: 'center' }} source={{uri:value.Poster}} />
//                 <Text style={{ color: 'white', alignSelf: 'center', fontSize: 17 }}>{value.Title}</Text>
//                 <Text style={{ color: 'white', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent: 'center',flexDirection: 'column' }}>{value.Year}</Text>
//                 <Text style={{ color: 'white', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent: 'center',flexDirection: 'column' }}>{value.Type}</Text>
//               </View>
//                 ))}
//                   <Text>{this.state.totalResult}</Text>
//                   <Text>{this.state.name}</Text>
//                   <Text>{this.state.alamat}</Text>
//             </View>
//           </View>
//         </View>
//       </ScrollView>
//     )
//   }
// }
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#4b0082',
//   },
// });