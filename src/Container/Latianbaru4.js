import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class Sans4 extends Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor:'blue'}}>
        <View style={{flex: .05, justifyContent: 'center', alignItems: 'center', marginBottom: 10}}>
          <Text style={{color:'white', fontSize: 17 }}> Parent Component </Text>
        </View>
        <View style={{flex: .1, flexDirection:'row', marginVertical: 20, marginHorizontal: 20,justifyContent:'center'}}>
          <View style={{width: 170, height:80, backgroundColor:'yellow'}}/>
        </View>
        <View style={{flexDirection:'column', marginTop:40, alignItems:'flex-start'}}>
            <View style={{backgroundColor: 'green', width: '80%', height: 50, marginBottom: 20, borderWidth: 2}}/>
            <View style={{backgroundColor: 'green', width: '80%', height: 50, marginBottom: 20, borderWidth: 2}}/>
        </View>
        <View style={{flexDirection:'column', marginTop:20, alignItems:'flex-end'}}>
            <View style={{backgroundColor: 'red', width: '80%', height: 50, marginBottom: 20, borderWidth: 2}}/>
            </View>
        </View>
    )
  }
}  
export default Sans4