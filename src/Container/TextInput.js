import React, {  Component  } from 'react'
import {  TextInput, View, StyleSheet} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import { skipPartiallyEmittedExpressions } from 'typescript'

export class TextInput extends Component {
    render() {
        return (

            <View style={styles.input}>
                <Icon name={this.props.name} size={20} style={{ color: 'green', marginRight: 10}} />
                <TextInput placeholder={this.props.placeholder} />
            </View>
        )
    }
}

export default TextInput

const styles = StyleSheet.create({
    input: {
        width: 200,
        height: 60,
        padding: 10,
        flexDirection: "row",
        borderWidth: 1,
        borderColor: 'black',
        elevation: 10,
        backgroundColor: '#ecf0f1',
        marginBottom: 20
    }
})