import React, { Component } from 'react'
import { TextInput, View, StyleSheet, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import { skipPartiallyEmittedExpressions } from 'typescript'

export class textInput extends Component {
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'blue' }}>
                <View style={{ flex: .1, justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
                    <Text style={{ color: 'black', fontSize: 13 }}> Parent Component </Text>
                </View>

                <View style={styles.input}>
                    
                    <Icon name={this.props.name} size={20} style={{ width: 80, height: 200,color: 'green', marginRight: 10 }} />
                    <TextInput placeholder={this.props.placeholder} />
                    <Text>Nama User</Text>
                    

                <View style={styles.input}>
                    <Icon name={this.props.name} size={20} style={{ color: 'green', marginRight: 10 }} />
                    <TextInput placeholder={this.props.placeholder} />
                    <Text>Nama User</Text>
                </View>
            </View>
            </View>

        )
    }
}

export default textInput

const styles = StyleSheet.create({
    input: {
        width: 200,
        height: 60,
        padding: 10,
        flexDirection: "row",
        borderWidth: 1,
        borderColor: 'white',
        elevation: 10,
        backgroundColor: 'white',
        marginBottom: 20
    }
})
