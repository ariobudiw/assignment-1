import React, { Component } from 'react'
import { Text, View ,StyleSheet, Button, TextInput, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/Entypo';

export class TableState extends Component {
    constructor(){
        super()
        this.state = {
            data:[
                {Nomor: 1,name:'Ario',address:'Pekanbaru'},
                {Nomor: 2,name:'Jono',address:'Jakarta'},
                {Nomor: 3,name:'Culeng',address:'Bekasi'},
            ],
            newNomor:'',
            newData:'',
            newAddress:'',
            newAksi:''
        }
    }

    tambah=()=>{
        const tampungan = this.state.data
        tampungan.push({id:this.state.newNomor,name:this.state.newData,address:this.state.newAddress,aksi:this.state.newAksi})
        this.setState({
            data: tampungan,
            newNomor:'',
            newData:'',
            newAddress:'',
            newAksi:''
        })
    }
    delete=(pareman)=>{
        const hapos = this.state.data.filter(data => data.Nomor != pareman.Nomor)
        this.setState({ 
            data:hapos
    })
}
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.table}>
                    <View style={styles.inTable}>
                        <Text style={styles.number}>Nomor</Text>
                        <Text style={styles.name}>Name</Text>
                        <Text style={styles.address}>Address</Text>
                        <Text style={styles.aksi}>Aksi</Text>
                    </View>
                    {this.state.data.map((value,index)=>{
                        return(
                            <View key={index} style={styles.Table}>
                                <Text style={[styles.number,styles.data]}>{index+1}</Text>
                                <Text style={[styles.name,styles.data]}>{value.name}</Text>
                                <Text style={[styles.address,styles.data]}>{value.address}</Text>
                                <Text style={[styles.address,styles.data]}>{value.aksi}</Text>
                    <TouchableOpacity onPress={()=>this.delete(value)}>
                    <Icon name='trash' size={25} color='black'/>
                    </TouchableOpacity>
                            </View>
                        )
                    })}
                </View>
            <View style={{backgroundColor:'#f0e68c',borderRadius:10,marginTop:20,alignItems:'center'}}>
                    <TextInput style={styles.input} placeholder='Nomor' onChangeText={(input)=>this.setState({newNomor : input})} value={this.state.newNomor}/>
                    <TextInput style={styles.input} placeholder='Nickname' onChangeText={(input)=>this.setState({newData : input})} value={this.state.newData}/>
                    <TextInput style={styles.input} placeholder='Address' onChangeText={(input)=>this.setState({newAddress : input})} value={this.state.newAddress}/>
                    <Button title='Add' onPress={this.tambah}/>    
                    
            </View>
            </View>

        )
    }
}

export default TableState

const styles = StyleSheet.create({
    container:{
        backgroundColor:'yellow',
        flex:1,
        justifyContent:'center'
    },
    table:{
        backgroundColor:'#ffd700',
        width:'90%',
        marginHorizontal:20,
    },
    inTable:{
        flexDirection:'row',
        justifyContent:'space-around',
        backgroundColor:'#ffd700',
        borderWidth:2,
        height:80,
        alignItems:'center',
    },
    Table:{
        flexDirection:'row',
        justifyContent:'space-around',
        borderWidth:1,
        alignItems:'center',
        height:80
    },
    name:{
        fontSize:17,
    },
    number:{
        fontSize:17,
    },
    address:{
        fontSize:17,
    },
    aksi:{
        fontSize:17,
    },
    input:{
        borderWidth:2,
        width:'50%',
        marginHorizontal:20,
        marginTop:20,
        borderRadius:5,
        backgroundColor:'yellow'
    }
})