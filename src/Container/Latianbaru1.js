import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class Sans1 extends Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor:'blue'}}>
        <View style={{flex: .05, justifyContent: 'center', alignItems: 'center', marginBottom: 10}}>
          <Text style={{color:'white', fontSize: 17 }}> Parent Component </Text>
        </View>
        
        <View style={{flex: .1, flexDirection:'row'}}>
          <View style={{flex: .35, backgroundColor:'pink'}}/>
          <View style={{flex: .35, backgroundColor:'green'}}/>
        </View>

        <View style={{flex: .1, flexDirection:'row', marginVertical: 20, justifyContent:'space-between'}}>
          <View style={{flex: .35, backgroundColor:'pink'}}/>
          <View style={{flex: .35, backgroundColor:'green'}}/>
        </View>

        <View style={{flex: .3, backgroundColor:'pink'}}>
          <View style={{flex: .3, justifyContent:'space-between', flexDirection:'row', marginBottom:10}}>
            <View style={{flex: .4, backgroundColor: 'green'}}/>
            <View style={{flex: .4, backgroundColor: 'green'}}/>
          </View>
          <View style={{flex: .3, flexDirection:'row', marginBottom:10}}>
            <View style={{flex: .4, backgroundColor: 'green'}}/>
            <View style={{flex: .4, backgroundColor: 'yellow'}}/>
          </View>

          <View style={{flex: .3, flexDirection:'row', justifyContent:'center', marginBottom: 10}}>
            <View style={{flex: .25, backgroundColor:'yellow'}}/>
            <View style={{flex: .25, backgroundColor:'green'}}/>
            <View style={{flex: .25, backgroundColor:'red'}}/>
          </View>
        </View>
      
      </View>
    )
  }
}

export default Sans1