import React, { Component } from 'react';
import { Alert, Button, TextInput, View, StyleSheet, Image, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class Sans6 extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
        };
    }

    onLogin = () => {
        const { username, password } = this.state;

        Alert.alert('Credentials', `${username} + ${password}`);
    }

    render() {
        return (


            <View style={styles.container}>
                <Image style={{ width: 10, height: 10, padding: 70 }}
                    resizeMode={'center'}
                    source={require('./src/assets/Salt1.png')} style={styles.logo} />
                    
                    <Text style={{ color: 'black', fontSize: 16 }}> Please login with a registered session </Text>
                    <View style={{ flex: .1, justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
                    </View>
                    
                <View style={styles.input}>
                    <Icon name="user" size={22} color="#000000" />
                    <TextInput
                        value={this.state.username}
                        onChangeText={(username) => this.setState({ username })}             
                        placeholder={'Username'}
                        placeholderTextColor={'black'}
                        color={'black'}
                    />
                </View>

                <View style={styles.input}>
                    <Icon name="key" size={22} color="#000000" />
                    <TextInput
                        value={this.state.username}
                        onChangeText={(username) => this.setState({ username })}
                        placeholder={'Password'}
                        placeholderTextColor={'black'}
                        color={'black'}
                    />
                </View>

                <Button
                    title={'Login'}
                    style={styles.input}
                    onPress={this.onLogin()}
                />
                <View style={{}}>
                    <Text>Forgot Password?</Text>

                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'yellow',
    },
    input: {
        width: 200,
        height: 60,
        padding: 10,
        flexDirection: "row",
        borderWidth: 1,
        borderColor: 'black',
        elevation: 10,
        backgroundColor: '#ecf0f1',
        marginBottom: 20,
    },
});