import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class Sans5 extends Component {
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'blue' }}>
                <View style={{ flex: .1, justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
                    <Text style={{ color: 'black', fontSize: 13 }}> Parent Component </Text>
                </View>
                <View style={{ marginBottom: 60, backgroundColor: 'pink', padding: '3%',alignSelf:'center', flexDirection: 'row' }}>
                
                    <View style={{ width: '25%', borderRadius: 100, backgroundColor: 'yellow', height: 100, marginTop: 40 }} >
                        <Text style={{ color: 'black', fontSize: 13, marginTop: 40, alignSelf: 'center' }}> Component </Text>
                    </View>
                    <View style={{ flexDirection: 'column', paddingLeft: '3%' }}>

                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ backgroundColor: 'green', width: '80%', height: 40, marginTop: 70, alignSelf: 'center', justifyContent: 'center' }} >
                                <Text style={{ color: 'black', fontSize: 13, alignSelf: 'center' }}> Component </Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ flex: .3, backgroundColor: 'pink' }}>
                    <Text style={{ color: 'black', fontSize: 13, justifyContent: 'center', alignSelf: 'center' }}> Child Component </Text>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', marginBottom: 1, marginTop: 40 }}>
                        <View style={{ flex: .1, flexDirection: 'row', justifyContent: 'center', marginVertical: 60 }} />
                        <View style={{ width: '20%', height: '60%', backgroundColor: 'yellow', marginHorizontal: 10 }} >
                            <Text style={{ color: 'black', fontSize: 13, }}> Component </Text>
                        </View>
                        <View style={{ width: '20%', height: '60%', backgroundColor: 'yellow', marginHorizontal: 10 }} >
                            <Text style={{ color: 'black', fontSize: 13, justifyContent: 'center', alignSelf: 'center' }}> Component </Text>
                        </View>
                        <View style={{ width: '20%', height: '60%', backgroundColor: 'yellow', marginHorizontal: 10 }} >
                            <Text style={{ color: 'black', fontSize: 13, alignSelf: 'center' }}> Component </Text>
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: 'column', marginTop: 40, alignItems: 'center' }}>
                    <View style={{ backgroundColor: 'green', width: '80%', height: 50, marginBottom: 20, borderWidth: 2 }} >
                        <Text style={{ color: 'black', fontSize: 13, justifyContent: 'center', alignSelf: 'center' }}> Component </Text>
                    </View>
                    <View style={{ backgroundColor: 'green', width: '80%', height: 50, marginBottom: 20, borderWidth: 2 }} >
                        <Text style={{ color: 'black', fontSize: 13, alignSelf: 'center' }}> Component </Text>
                    </View>
                </View>

            </View>
        )
    }
}
export default Sans5