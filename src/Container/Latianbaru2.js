import React, { Component } from 'react'
import { Text, View } from 'react-native'

export class Sans2 extends Component {
  render() {
    return (
      <View style={{flex: 1, backgroundColor:'blue', paddingHorizontal: '2%', height: '100%'}}>
        <View style={{flex: .05, justifyContent: 'center', alignItems: 'center', marginBottom: 10}}>
          <Text style={{color:'white', fontSize: 17 }}> Parent Component </Text>
        </View>
        
        <View style={{width: '100%', height: 150, backgroundColor:'pink', padding:'3%', alignItems:'center', flexDirection: 'row'}}>
          <View style={{width: '25%', borderRadius: 100, backgroundColor: 'yellow', height: 100}}/>
          <View style={{flexDirection: 'column', paddingLeft: '3%'}}>
            <View style={{backgroundColor: 'green', width: '100%', height: 40, marginTop: 100}} />
            <View style={{flexDirection: 'row'}}>
              <View style={{backgroundColor: 'red', width: '35%', height: 40, marginBottom: 100}} />
              <View style={{backgroundColor: 'yellow', width: '35%', height: 40}} />
            </View>
          </View>
        </View>
        
        <View style={{flexDirection:'column', marginTop:20, alignItems:'flex-start'}}>
            <View style={{backgroundColor: 'green', width: '80%', height: 50, marginBottom: 20, borderWidth: 2}}/>
            <View style={{backgroundColor: 'green', width: '80%', height: 50, marginBottom: 20, borderWidth: 2}}/>
            <View style={{backgroundColor: 'green', width: '80%', height: 50, marginBottom: 20}}/>
            </View>
          
          <View style={{flexDirection:'column', marginTop:20, alignItems:'flex-end'}}>
            <View style={{backgroundColor: 'yellow', width: '80%', height: 50, marginBottom: 20, borderWidth: 2}}/>
            <View style={{backgroundColor: 'yellow', width: '80%', height: 50, marginBottom: 20, borderWidth: 2}}/>
            <View style={{backgroundColor: 'yellow', width: '80%', height: 50, marginBottom: 20}}/>
                </View>
            </View>  
             
    )
  }
}

export default Sans2