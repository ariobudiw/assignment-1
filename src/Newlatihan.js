import React, { Component } from 'react'
import { Text, View, Image, StyleSheet, ScrollView } from 'react-native'


export default class Baru1 extends Component {
  constructor() {
    super()
    this.state = { 
      name:'ario',
      totalResult:'0', 
      alamat:'gempol', 
      data: [],
    }
  }

  componentDidMount = () => {
    console.log([0,1,2,3,4,5,6,7,8,9,19] )
    console.log(typeof View)
  
    fetch('http://www.omdbapi.com/?s=avengers&apikey=997061b4&')
      .then(response => response.json())
      .then(jjson => {
        console.log(jjson.Search[0].Year)
        this.setState({ data: [jjson.Search[8],jjson.Search[7],jjson.Search[6]] })
      })

      this.setState({ name:'', alamat:'gempol'})
  }

  render() {
    return (
      <ScrollView style={styles.scrollView}>
        <View style={{ flex: 1, backgroundColor: 'white' }}>
          <View style={styles.container}>
            <View style={{ flex: .05, justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ width: 400, height: 120, backgroundColor: '#191970', marginTop: 20 }}>
                <Text style={{ color: 'white', fontSize: 50, marginTop: 20, alignSelf: 'center', justifyContent: 'center' }}> Movies card </Text>
              </View>
            </View>
            <View style={{ flex: .1, flexDirection: 'column', marginVertical: 20, justifyContent: 'space-between', marginLeft: 10, marginRight: 10, marginBottom: 10 }}>
            {this.state.data.map((value, index) => (
              <View key={index} style={{ width: 330, height: 400, backgroundColor: '#191970' }}>
                <Image style={{ width: '100%', height: "80%", justifyContent: 'center' }} source={{uri:value.Poster}} />
                <Text style={{ color: 'white', alignSelf: 'center', fontSize: 17 }}>{value.Title}</Text>
                <Text style={{ color: 'white', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent: 'center',flexDirection: 'column' }}>{value.Year}</Text>
                <Text style={{ color: 'white', fontSize: 17, marginTop: 1, alignSelf: 'center', justifyContent: 'center',flexDirection: 'column' }}>{value.Type}</Text>
              </View>
                ))}
                  <Text>{this.state.totalResult}</Text>
                  <Text>{this.state.name}</Text>
                  <Text>{this.state.alamat}</Text>
            </View>
          </View>
        </View>
      </ScrollView>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#4b0082',
  },
});