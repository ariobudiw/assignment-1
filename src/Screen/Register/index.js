import { NavigationHelpersContext } from '@react-navigation/native';
import * as React from 'react';
import { Alert, Button, TextInput, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

export default class Register extends React.Component {

    render() {
        return (
            
            
            <View style={styles.container}>
                
                <Text style={{ color: 'black', fontSize: 13,marginBottom:12 }}> Please Register with valid data </Text>
                <View style={styles.input}>
                    <Icon name="user" size={22} color="#000000" />
                    <TextInput
                        placeholder='Fullname'
                        placeholderTextColor='black'
                        color='black'
                        
                    />
                </View>
                <View style={styles.input}>
                    <Icon name="user" size={22} color="#000000" />
                    <TextInput
                        placeholder='Username'
                        placeholderTextColor='black'
                        color='black'
                    />
                </View>
                <View style={styles.input}>
                    <Icon name="envelope" size={22} color="#000000" />
                    <TextInput
                        placeholder='Email'
                        placeholderTextColor='black'
                        color='black'
                    />
                </View>
                <View style={styles.input}>
                    <Icon name="key" size={22} color="#000000" />
                    <TextInput
                        placeholder='Password'
                        placeholderTextColor='black'
                        color='black'
                    />
                </View>
                <View style={styles.input}>
                    <Icon name="key" size={22} color="#000000" />
                    <TextInput
                        placeholder='Confirm Password'
                        placeholderTextColor='black'
                        color='black'
                    />
                </View>
                <Button title="Go to Details" onPress={()=>this.props.navigation.navigate('Details')}/>z
                <TouchableOpacity onPress={()=>alert('Masukkan email')}>
                    <Text>Forgot Password?</Text>
                    
                </TouchableOpacity>
            </View>
        );
    }                            //Latian7
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'yellow',
    },
    input: {
        alignItems:'center',
        width: 220,
        height: 60,
        padding: 10,
        flexDirection: "row",
        borderWidth: 2,
        borderColor: 'black',
        elevation: 10,
        backgroundColor: '#ecf0f1',
        marginBottom: 20,
        
    },
});
