import React,  {Component} from 'react';
import {View, Text, TouchableOpacity, ImageBackground, Image} from 'react-native';

export default class LoadingScreen extends React.Component {
    constructor() {
        super();
        this.state = {};
    }
    componentDidMount() {
      setTimeout(() => {
        this.props.navigation.replace('Home');
      }, 2000);
  }
  

  render(){
      return (
              <ImageBackground style={{flex:1,justifyContent:'center',alignItems:'center',backgroundColor:'#00ff7f'}} >
              <Image style={{width:250,height:200}} source={require('../victor.png')}></Image>
              </ImageBackground>
      )
  }
}