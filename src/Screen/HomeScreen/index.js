import { NavigationHelpersContext } from '@react-navigation/native';
import * as React from 'react';
import { Alert, Button, TextInput, View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';


export default class HomeScreen extends React.Component {
constructor(){
    super();
    this.state = {showPassword:true}
}
show=()=>{
    this.setState({
        showPassword: !this.state.showPassword
    })
}

buttonPress=()=>{
    this.setState({
        email:'',
        password:'',
    })
}
    render() {
        const {showPassword} = this.state
        return (
            
            
            <View style={styles.container}>
                <Image style={{ width: '50%', height: "17%", alignItems:'center', marginBottom:20, }} source={require('../victor.png')} />
                <Text style={{ color: 'black', fontSize: 20,marginBottom:40 }}> Please Register with valid data! </Text>
                <View style={styles.input}>
                    <Icon name="user" size={22} color="#000000" />
                    <TextInput
                        placeholder='Fullname'
                        placeholderTextColor='black'
                        color='black'
                        
                    />
                </View>
                <View style={styles.input}>
                    <Icon name="user" size={22} color="#000000" />
                    <TextInput
                        placeholder='Username'
                        placeholderTextColor='black'
                        color='black'
                    />
                </View>
                <View style={styles.input}>
                    <Icon name="mail" size={22} color="#000000" />
                    <TextInput
                        placeholder='Email'
                        placeholderTextColor='black'
                        color='black'
                    />
                </View>
                <View style={styles.input}>
                    <Icon name="key" size={22} color="#000000" />
                    <TextInput value={this.state.password} secureTextEntry={showPassword} placeholder='Password' placeholderTextColor='black' color='black' onChangeText ={(typing)=>this.setState({password:typing})}style={{flex:1}}/>
                    <TouchableOpacity onPress={this.show}>
                        <Icon name='eye' size={30} color='black'/>
                    </TouchableOpacity>
                </View>
                
                <Button title="Register" onPress={()=>this.props.navigation.navigate('Details')}/>
                
            </View>
        );
    }                            
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#00ff7f',
    },
    input: {
        alignItems:'center',
        width: 220,
        height: 60,
        padding: 10,
        flexDirection: "row",
        borderWidth: 2,
        borderColor: 'black',
        elevation: 10,
        backgroundColor: '#ecf0f1',
        marginBottom: 20,
        borderRadius:17
        
    },
});
